import * as Sentry from '@sentry/node';
import LoggerTransport from "snakeoil-logger/src/LoggerTransport.js"
// documentation: https://docs.sentry.io/platforms/javascript/
// documentation: https://docs.sentry.io/platforms/javascript/usage/
class SentryIoTransport extends LoggerTransport {

    // documentation: https://docs.sentry.io/platforms/javascript/configuration/options/
    constructor(options) {
        super(options)
        Sentry.init(options)
        this.sentry = Sentry
    }

    write(any) {
        if(!any) return;
        if(any.cause){
            Sentry.captureException(any.cause)
        } else {
            Sentry.captureException(any.message)
        }
    }

    getInstance(){
        return this.sentry
    }
}

export default SentryIoTransport